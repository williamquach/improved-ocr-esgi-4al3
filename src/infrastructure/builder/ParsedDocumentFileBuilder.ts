import { ParsedDocumentBuilder } from '../../domain/builder/ParsedDocumentBuilder';
import { ParsedDocument } from '../../domain/models/parsing/ParsedDocument';
import { ParsedDocumentFile } from '../models/parsing/ParsedDocumentFile';

export class ParsedDocumentFileBuilder extends ParsedDocumentBuilder {
    setEntries(entriesByLine: string[]): this {
        if (!entriesByLine) {
            this.entriesByLine = [];
            return this;
        }
        this.entriesByLine.push(this.parsedDocumentEntriesByLineBuilder.setEntries(entriesByLine).build());
        return this;
    }

    build(): ParsedDocument {
        return new ParsedDocumentFile(this.entriesByLine);
    }
}

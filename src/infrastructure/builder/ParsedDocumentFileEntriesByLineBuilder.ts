import { ParsedDocumentEntriesByLineBuilder } from '../../domain/builder/ParsedDocumentEntriesByLineBuilder';
import { ParsedDocumentFileEntriesByLine } from '../models/parsing/ParsedDocumentFileEntriesByLine';

export class ParsedDocumentFileEntriesByLineBuilder extends ParsedDocumentEntriesByLineBuilder {
    setEntries(entries: string[]): this {
        this.entries = entries;
        return this;
    }

    build(): ParsedDocumentFileEntriesByLine {
        return new ParsedDocumentFileEntriesByLine(this.entries ?? []);
    }
}

import * as fs from 'fs';
import { FileService } from '../../domain/services/FileService';
import path from 'path';

export class FSFileService implements FileService {
    getFileContent(filePath: string): string {
        return fs.readFileSync(filePath, 'utf8');
    }

    writeInFile(filePath: string, fileName: string, content: string): string {
        fs.mkdirSync(filePath, { recursive: true });
        const fullFilePath = path.join(filePath, fileName);
        fs.writeFileSync(fullFilePath, content, { encoding: 'utf8', flag: 'a' });
        return fullFilePath;
    }

    removeFilesInOutputFolder(folderPath: string) {
        this.deleteFolderRecursive(folderPath);
    }

    private deleteFolderRecursive = (directoryPath: string) => {
        if (fs.existsSync(directoryPath)) {
            fs.readdirSync(directoryPath).forEach(file => {
                const currentPath = path.join(directoryPath, file);
                if (fs.lstatSync(currentPath).isDirectory()) {
                    this.deleteFolderRecursive(currentPath);
                    fs.rmdirSync(currentPath);
                } else {
                    fs.unlinkSync(currentPath);
                }
            });
        }
    };
}

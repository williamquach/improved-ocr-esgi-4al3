import { ImprovedOCRUseCases } from '../../../domain/use-cases/ocr/ImprovedOCRUseCases';
import { OutputFilePaths } from '../../models/output/OutputFilePaths';
import { ImprovedOCRRules } from '../../../domain/models/ImprovedOCRRules';
import { FileService } from '../../../domain/services/FileService';
import { ParsedDocumentBuilder } from '../../../domain/builder/ParsedDocumentBuilder';
import { ImprovedOCR } from '../../../domain/models/ImprovedOCR';
import { FileGroupedImprovedOCR } from '../../models/FileGroupedImprovedOCR';
import { FileImprovedOCR } from '../../models/FileImprovedOCR';

export class FileImprovedOCRUseCases implements ImprovedOCRUseCases {
    private improvedOCR: ImprovedOCR;

    constructor(
        private rules: ImprovedOCRRules,
        protected fileService: FileService,
        private parsedDocumentBuilder: ParsedDocumentBuilder,
        groupFiles: boolean
    ) {
        if (groupFiles) {
            this.improvedOCR = new FileGroupedImprovedOCR(rules, fileService, parsedDocumentBuilder);
        } else {
            this.improvedOCR = new FileImprovedOCR(rules, fileService, parsedDocumentBuilder);
        }
    }

    readFileAndWriteOutput(filePaths: string[], outputDirectoryPath: string): OutputFilePaths {
        const fileContents = this.improvedOCR.readDocuments(filePaths);
        const parsedDocuments = this.improvedOCR.parseDocuments(fileContents);
        return this.improvedOCR.produceFilesFromEntryFiles(outputDirectoryPath, parsedDocuments);
    }
}

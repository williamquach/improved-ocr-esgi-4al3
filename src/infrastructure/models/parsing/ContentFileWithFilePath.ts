export interface ContentFileWithFilePath {
    filePath: string;
    content: string;
}

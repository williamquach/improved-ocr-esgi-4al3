import { ParsedDocument } from '../../../domain/models/parsing/ParsedDocument';

export interface ParsedDocumentWithFilePath {
    filePath: string;
    parsedDocument: ParsedDocument;
}

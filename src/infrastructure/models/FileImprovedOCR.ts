import { ParsedDocumentBuilder } from '../../domain/builder/ParsedDocumentBuilder';
import { ImprovedOCR } from '../../domain/models/ImprovedOCR';
import { FileService } from '../../domain/services/FileService';
import { ImprovedOCRRules } from '../../domain/models/ImprovedOCRRules';
import { ParsedDocumentEntriesByLine } from '../../domain/models/parsing/ParsedDocumentEntriesByLine';
import { CheckSumValidation } from '../../domain/enums/CheckSumValidation';
import { ParsedDocumentWithFilePath } from './parsing/ParsedDocumentWithFilePath';
import { ContentFileWithFilePath } from './parsing/ContentFileWithFilePath';
import { DigitRepresentationByNumber } from '../../domain/models/DigitRepresentationByNumber';
import { OutputFilePaths } from './output/OutputFilePaths';

export class FileImprovedOCR implements ImprovedOCR {
    private readonly digitRepresentationByNumber: DigitRepresentationByNumber = {
        0: [' _ ', '| |', '|_|'],
        1: ['   ', '  |', '  |'],
        2: [' _ ', ' _|', '|_ '],
        3: [' _ ', ' _|', ' _|'],
        4: ['   ', '|_|', '  |'],
        5: [' _ ', '|_ ', ' _|'],
        6: [' _ ', '|_ ', '|_|'],
        7: [' _ ', '  |', '  |'],
        8: [' _ ', '|_|', '|_|'],
        9: [' _ ', '|_|', ' _|'],
    };

    constructor(
        private rules: ImprovedOCRRules,
        protected fileService: FileService,
        private parsedDocumentBuilder: ParsedDocumentBuilder
    ) {}

    static isBlank(line: string) {
        return line.trim().length === 0;
    }

    private static areSameRepresentation(block: string[], digitRepresentationByNumber: string[]) {
        if (block.length !== digitRepresentationByNumber.length) {
            throw new Error('File is not correctly formatted. See number of lines of blocks in file.');
        }
        for (let i = 0; i < block.length; i += 1) {
            const blockLine = block[i];
            const digitRepresentationLine = digitRepresentationByNumber[i];
            for (let j = 0; j < blockLine.length; j += 1) {
                const blockLineChar = blockLine[j];
                const digitRepresentationChar = digitRepresentationLine[j];
                if (blockLineChar !== digitRepresentationChar) {
                    return false;
                }
            }
        }
        return true;
    }

    readDocuments(filePaths: string[]): ContentFileWithFilePath[] {
        const filesContent: ContentFileWithFilePath[] = [];
        filePaths.forEach(filePath => {
            filesContent.push({
                filePath: filePath,
                content: this.fileService.getFileContent(filePath),
            });
        });
        return filesContent;
    }

    parseDocuments(filesContent: ContentFileWithFilePath[]): ParsedDocumentWithFilePath[] {
        const parsedDocuments: ParsedDocumentWithFilePath[] = [];

        for (const fileContent of filesContent) {
            const parsedDocument = this.parsedDocumentBuilder.setEntries().build();

            const lineByLine = fileContent.content.split('\n');
            this.rules.respectAll(lineByLine);

            const allLinesOfDigitBlocks = this.getAllLinesOfDigitBlocks(lineByLine);
            for (const currentLineOfDigitBlock of allLinesOfDigitBlocks) {
                const blockOutputRepresentation: string[] = [];
                for (const block of currentLineOfDigitBlock) {
                    let recognised = false;
                    for (const key in this.digitRepresentationByNumber) {
                        if (FileImprovedOCR.areSameRepresentation(block, this.digitRepresentationByNumber[key])) {
                            blockOutputRepresentation.push(key);
                            recognised = true;
                            break;
                        }
                    }
                    if (!recognised) {
                        blockOutputRepresentation.push('?');
                    }
                }
                parsedDocument.push(
                    this.parsedDocumentBuilder.parsedDocumentEntriesByLineBuilder
                        .setEntries(blockOutputRepresentation)
                        .build()
                );
            }
            parsedDocuments.push({
                filePath: fileContent.filePath,
                parsedDocument,
            });
        }

        return parsedDocuments;
    }

    checkSum(currentLineEntries: ParsedDocumentEntriesByLine): CheckSumValidation {
        let invertedIndex = currentLineEntries.entriesByLine.length;
        let total = 0;
        currentLineEntries.entriesByLine.forEach(entry => {
            const entryValue: number = parseInt(entry);
            total += entryValue * invertedIndex;
            invertedIndex -= 1;
        });
        const result = total % this.rules.CHECK_SUM_MODULO_VALUE === 0;

        if (Number.isNaN(total)) {
            return CheckSumValidation.UNREADABLE;
        } else if (result) {
            return CheckSumValidation.VALID;
        } else {
            return CheckSumValidation.INVALID;
        }
    }

    produceFilesFromEntryFiles(
        filePath: string,
        parsedDocumentWithFilePath: ParsedDocumentWithFilePath[]
    ): OutputFilePaths {
        const writtenFilePaths: OutputFilePaths = new OutputFilePaths();
        const isoDatetime = new Date().toISOString();

        for (const parsedDocumentWithFileName of parsedDocumentWithFilePath) {
            let content = '';
            for (const currentLineEntries of parsedDocumentWithFileName.parsedDocument.entriesByLine) {
                content += `${currentLineEntries.toString()}${this.getSecondColumnContent(currentLineEntries)}`;
            }

            const filePathSplit = parsedDocumentWithFileName.filePath.split('/');
            const fileName = filePathSplit[filePathSplit.length - 1];
            const outputFileName = `${isoDatetime}_${fileName}`;
            writtenFilePaths.push({ filePath: this.fileService.writeInFile(filePath, outputFileName, content) });
        }

        return writtenFilePaths;
    }

    protected getSecondColumnContent(currentLineEntries: ParsedDocumentEntriesByLine): string {
        const checkSum = this.checkSum(currentLineEntries);
        return checkSum === CheckSumValidation.VALID ? '\n' : ` ${checkSum}\n`;
    }

    private getAllLinesOfDigitBlocks(lines: string[]): string[][][] {
        lines = this.ejectBlankLines(lines);
        const allLinesOfDigitBlocks = [];
        for (let entryIndex = 0; entryIndex < lines.length; entryIndex += 3) {
            const currentLines = lines.filter((line, index) => {
                return index === entryIndex || index === entryIndex + 1 || index === entryIndex + 2;
            });
            const currentLineDigitBlocks = this.getAllDigitBlocks(currentLines);
            allLinesOfDigitBlocks.push(currentLineDigitBlocks);
        }
        return allLinesOfDigitBlocks;
    }

    private getAllDigitBlocks(lines: string[]): string[][] {
        const allBlocks = [];

        let numberOfBlocks = this.getNumberOfBlocks(lines);
        this.rules.shouldRespectMaxNumberOfDigitByEntry(numberOfBlocks);
        for (let i = 0; i < numberOfBlocks; i += 1) {
            allBlocks.push(this.getDigitBlockByBlockIndex(lines, i * 3));
        }
        return allBlocks;
    }

    private ejectBlankLines = (lines: string[]): string[] => {
        return lines.filter((line, index) => (index + 1) % this.rules.ENTRY_LINE_COUNT !== 0);
    };

    private getNumberOfBlocks = (lines: string[]): number =>
        Math.floor(Math.max(...lines.map(line => line.length)) / 3);

    private getDigitBlockByBlockIndex(lines: string[], blocIndex: number): string[] {
        return lines.map(line => {
            let bloc = '';
            for (let i = blocIndex; i < blocIndex + 3; i += 1) {
                bloc += line[i] ?? '';
            }
            return bloc;
        });
    }
}

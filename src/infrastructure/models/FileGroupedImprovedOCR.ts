import { FileImprovedOCR } from './FileImprovedOCR';
import { ParsedDocumentWithFilePath } from './parsing/ParsedDocumentWithFilePath';
import { CheckSumValidation } from '../../domain/enums/CheckSumValidation';
import { OutputFilePaths } from './output/OutputFilePaths';

interface CodeContentWithFileName {
    content: string;
    fileName: string;
}

export class FileGroupedImprovedOCR extends FileImprovedOCR {
    produceFilesFromEntryFiles(
        filePath: string,
        parsedDocumentWithFilePath: ParsedDocumentWithFilePath[]
    ): OutputFilePaths {
        const writtenFilePaths: OutputFilePaths = new OutputFilePaths();
        const isoDatetime = new Date().toISOString();
        for (const parsedDocumentWithFileName of parsedDocumentWithFilePath) {
            let authorizedContent: CodeContentWithFileName = {
                content: '',
                fileName: 'authorized.txt',
            };
            let erroredContent: CodeContentWithFileName = {
                content: '',
                fileName: 'errored.txt',
            };
            let unknownContent: CodeContentWithFileName = {
                content: '',
                fileName: 'unknown.txt',
            };

            for (const currentLineEntries of parsedDocumentWithFileName.parsedDocument.entriesByLine) {
                let content = `${currentLineEntries.toString()}${this.getSecondColumnContent(currentLineEntries)}`;
                if (this.checkSum(currentLineEntries) === CheckSumValidation.VALID) {
                    authorizedContent.content += content;
                } else if (this.checkSum(currentLineEntries) === CheckSumValidation.INVALID) {
                    erroredContent.content += content;
                } else if (this.checkSum(currentLineEntries) === CheckSumValidation.UNREADABLE) {
                    unknownContent.content += content;
                }
            }

            writtenFilePaths.push({
                filePath: this.fileService.writeInFile(
                    `${filePath}/${isoDatetime}`,
                    authorizedContent.fileName,
                    authorizedContent.content
                ),
            });
            writtenFilePaths.push({
                filePath: this.fileService.writeInFile(
                    `${filePath}/${isoDatetime}`,
                    erroredContent.fileName,
                    erroredContent.content
                ),
            });
            writtenFilePaths.push({
                filePath: this.fileService.writeInFile(
                    `${filePath}/${isoDatetime}`,
                    unknownContent.fileName,
                    unknownContent.content
                ),
            });
        }

        return writtenFilePaths;
    }
}

import { ImprovedOCRRules } from '../../domain/models/ImprovedOCRRules';
import { EntryLineCountError } from '../../domain/errors/ocr/EntryLineCountError';
import { EntryLastLineNotBlankError } from '../../domain/errors/ocr/EntryLastLineNotBlankError';
import { IncorrectNumberOfEntriesError } from '../../domain/errors/ocr/IncorrectNumberOfEntriesError';
import { FileImprovedOCR } from './FileImprovedOCR';
import { EntryDigitCountError } from '../../domain/errors/ocr/EntryDigitCountError';

export class FileImprovedOCRRules implements ImprovedOCRRules {
    MAX_NUMBER_OF_DIGIT_IN_AN_ENTRY = 9;
    CHECK_SUM_MODULO_VALUE = 11;
    ENTRY_LINE_COUNT = 4;
    VERTICAL_SIZE_OF_DIGIT_IN_AN_ENTRY = 3;
    MAXIMUM_NUMBER_OF_ENTRIES = 100;

    respectAll(lines: string[]) {
        this.shouldRespectMaxNumberOfEntry(lines);
        this.shouldRespectEntryLineCount(lines);
        this.shouldRespectBlankLineAtEntryEnd(lines);
    }

    shouldRespectEntryLineCount(lines: string[]): void {
        if (lines.length % this.ENTRY_LINE_COUNT !== 0) {
            throw new EntryLineCountError(
                `Entries must be ${this.ENTRY_LINE_COUNT} lines each and end with only one blank line. `
            );
        }
    }

    shouldRespectBlankLineAtEntryEnd(lines: string[]): void {
        for (let lineIndex = 0; lineIndex < lines.length; lineIndex += this.ENTRY_LINE_COUNT) {
            const lastLineOfBlock = lines[lineIndex + (this.ENTRY_LINE_COUNT - 1)];
            if (!FileImprovedOCR.isBlank(lastLineOfBlock)) {
                throw new EntryLastLineNotBlankError("Last entries' line should be blank.");
            }
        }
    }

    shouldRespectMaxNumberOfEntry(lines: string[]): void {
        if (lines.length >= this.MAXIMUM_NUMBER_OF_ENTRIES * this.ENTRY_LINE_COUNT) {
            throw new IncorrectNumberOfEntriesError(
                `A file cannot contain more than ${this.MAXIMUM_NUMBER_OF_ENTRIES} entries.`
            );
        }
    }

    shouldRespectMaxNumberOfDigitByEntry(numberOfDigitInEntry: number): void {
        if (numberOfDigitInEntry > this.MAX_NUMBER_OF_DIGIT_IN_AN_ENTRY) {
            throw new EntryDigitCountError(
                `Entries cannot contain more than ${this.MAX_NUMBER_OF_DIGIT_IN_AN_ENTRY} digits.`
            );
        }
    }
}

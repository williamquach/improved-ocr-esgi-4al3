import { OutputFilePath } from './OutputFilePath';

export class OutputFilePaths {
    filePaths: OutputFilePath[] = [];

    push(outputFilePath: OutputFilePath) {
        if (!this.exists(outputFilePath)) {
            this.filePaths.push(outputFilePath);
        }
    }

    private exists(outputFilePath: OutputFilePath) {
        return this.filePaths.find(currentOutputFilePath => currentOutputFilePath.filePath === outputFilePath.filePath);
    }
}

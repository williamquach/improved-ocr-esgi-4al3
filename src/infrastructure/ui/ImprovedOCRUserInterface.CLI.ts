import { OutputFilePaths } from '../models/output/OutputFilePaths';
import { ImprovedOCRUserInterface } from '../../domain/ui/ImprovedOCRUserInterface';
import { ImprovedOCRUseCases } from '../../domain/use-cases/ocr/ImprovedOCRUseCases';
import { FileImprovedOCRUseCases } from '../use-cases/ocr/FileImprovedOCRUseCases';
import { ImprovedOCRRules } from '../../domain/models/ImprovedOCRRules';
import { FileService } from '../../domain/services/FileService';
import { ParsedDocumentBuilder } from '../../domain/builder/ParsedDocumentBuilder';

const prompts = require('prompts');

export class ImprovedOCRUserInterfaceCLI implements ImprovedOCRUserInterface {
    constructor(
        private rules: ImprovedOCRRules,
        private fileService: FileService,
        private parsedDocumentBuilder: ParsedDocumentBuilder
    ) {}

    private static async askUserWantsToRestart(): Promise<boolean> {
        const response = await prompts({
            type: 'select',
            name: 'wantsToRestart',
            message: 'Do you want retry scanning files with our improved OCR ?',
            choices: [
                { title: 'yes', value: 'y' },
                { title: 'no', value: 'n' },
            ],
        });
        return response.wantsToRestart === 'y';
    }

    private static async pauseBeforeNextStep(message: string) {
        await prompts({
            type: 'text',
            name: 'nextStep',
            message: `${message}\nPress enter to continue...\n`,
        });
    }

    async start(): Promise<OutputFilePaths | undefined> {
        console.log('\n\nWelcome to your favorite OCR!');
        await ImprovedOCRUserInterfaceCLI.pauseBeforeNextStep('');

        const outputFolderPath = await this.askUserFileOutputFolderPath();
        const userFilePaths = await this.askUserFilePaths();
        const wantsToGroupFile = await this.doesUserWantsToGroupFiles();
        const writtenFilePaths = await this.readFileAndWriteOutput(userFilePaths, outputFolderPath, wantsToGroupFile);
        if (writtenFilePaths) {
            console.log('Written file paths : ');
            console.log(writtenFilePaths.filePaths.map(filePath => filePath.filePath).join('\n'));
        }
        this.stop();
        return writtenFilePaths;
    }

    async askUserFileOutputFolderPath(): Promise<string> {
        while (true) {
            const response = await prompts({
                type: 'text',
                name: 'outputFolderPath',
                message: 'Where do you want to store output files ? (precise absolute path of folder)',
                validate: (outputFolderPath: any) => (outputFolderPath === '' ? `Folder path cannot be empty` : true),
            });
            if (Object.keys(response).length === 0) {
                this.stop();
            }
            if (response.outputFolderPath !== '') {
                return response.outputFolderPath;
            }
        }
    }

    async askUserFilePaths(): Promise<string[]> {
        const filePaths: string[] = [];
        while (true) {
            const response = await prompts({
                type: 'text',
                name: 'filePath',
                message:
                    'Enter a file absolute path that you want to scan with OCR (directly press enter to stop adding files) :',
                validate: (filePath: any) =>
                    filePath === '' && filePaths.length === 0 ? `You must add at least one file to scan!` : true,
            });
            if (Object.keys(response).length === 0) {
                this.stop();
            }
            if (response.filePath === '') {
                return filePaths;
            }
            filePaths.push(response.filePath);
        }
    }

    async doesUserWantsToGroupFiles(): Promise<boolean> {
        const response = await prompts({
            type: 'select',
            name: 'wantsToGroupFiles',
            message: 'Do you want to group output files by code (authorized, errored or unknown) ? (y/n)',
            choices: [
                { title: 'yes', value: 'y' },
                { title: 'no', value: 'n' },
            ],
        });
        return response.wantsToGroupFiles === 'y';
    }

    async readFileAndWriteOutput(
        filePaths: string[],
        outputFolderPath: string,
        wantsToGroupFile: boolean
    ): Promise<OutputFilePaths | undefined> {
        const improvedOCRUseCases: ImprovedOCRUseCases = new FileImprovedOCRUseCases(
            this.rules,
            this.fileService,
            this.parsedDocumentBuilder,
            wantsToGroupFile
        );
        try {
            console.log('\n\nReading file(s) and creating output file(s)...\n');
            return improvedOCRUseCases.readFileAndWriteOutput(filePaths, outputFolderPath);
        } catch (error: any) {
            console.log(`/!\\ - /!\\ - /!\\`);
            console.log(`An error has occurred during scanning process : ${error.message}\n\n`);
            const wantsToRestart = await ImprovedOCRUserInterfaceCLI.askUserWantsToRestart();
            if (wantsToRestart) {
                return await this.start();
            }
            this.stop();
            return undefined;
        }
    }

    stop(): void {
        console.log('------------------------------');
        console.log('See you soon to scan other files!!!');
        console.log('------------------------------');
        process.exit(1);
    }

    restart(): void {
        console.log('\n-------------');
        console.log('Restarting...');
    }
}

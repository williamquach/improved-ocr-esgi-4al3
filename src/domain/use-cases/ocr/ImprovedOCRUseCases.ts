import { OutputFilePaths } from '../../../infrastructure/models/output/OutputFilePaths';

export interface ImprovedOCRUseCases {
    readFileAndWriteOutput(filePaths: string[], outputDirectoryPath: string): OutputFilePaths;
}

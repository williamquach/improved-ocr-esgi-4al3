import { ImprovedOCRInputFormatError } from './ImprovedOCRInputFormatError';

export class EntryLineCountError extends ImprovedOCRInputFormatError {
    constructor(msg: string) {
        super(msg);
        Object.setPrototypeOf(this, EntryLineCountError.prototype);
    }
}

import { ImprovedOCRInputFormatError } from './ImprovedOCRInputFormatError';

export class EntryDigitCountError extends ImprovedOCRInputFormatError {
    constructor(msg: string) {
        super(msg);
        Object.setPrototypeOf(this, EntryDigitCountError.prototype);
    }
}

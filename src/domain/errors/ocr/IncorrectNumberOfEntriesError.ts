import { ImprovedOCRInputFormatError } from './ImprovedOCRInputFormatError';

export class IncorrectNumberOfEntriesError extends ImprovedOCRInputFormatError {
    constructor(msg: string) {
        super(msg);
        Object.setPrototypeOf(this, IncorrectNumberOfEntriesError.prototype);
    }
}

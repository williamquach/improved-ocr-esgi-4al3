import { ImprovedOCRInputFormatError } from './ImprovedOCRInputFormatError';

export class EntryLastLineNotBlankError extends ImprovedOCRInputFormatError {
    constructor(msg: string) {
        super(msg);
        Object.setPrototypeOf(this, EntryLastLineNotBlankError.prototype);
    }
}

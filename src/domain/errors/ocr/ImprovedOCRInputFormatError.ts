export class ImprovedOCRInputFormatError extends Error {
    constructor(msg: string) {
        super(msg);
        Object.setPrototypeOf(this, ImprovedOCRInputFormatError.prototype);
    }
}

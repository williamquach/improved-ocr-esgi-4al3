import { ParsedDocument } from '../models/parsing/ParsedDocument';
import { ParsedDocumentEntriesByLine } from '../models/parsing/ParsedDocumentEntriesByLine';
import { ParsedDocumentEntriesByLineBuilder } from './ParsedDocumentEntriesByLineBuilder';

export abstract class ParsedDocumentBuilder {
    protected entriesByLine: ParsedDocumentEntriesByLine[] = [];

    constructor(public parsedDocumentEntriesByLineBuilder: ParsedDocumentEntriesByLineBuilder) {}

    abstract setEntries(entriesByLine?: string[]): this;

    abstract build(): ParsedDocument;
}

import { ParsedDocumentEntriesByLine } from '../models/parsing/ParsedDocumentEntriesByLine';

export abstract class ParsedDocumentEntriesByLineBuilder {
    protected entries: string[] = [];

    abstract setEntries(entries: string[]): this;

    abstract build(): ParsedDocumentEntriesByLine;
}

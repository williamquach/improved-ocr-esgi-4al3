export interface ImprovedOCRRules {
    MAX_NUMBER_OF_DIGIT_IN_AN_ENTRY: number;
    CHECK_SUM_MODULO_VALUE: number;
    ENTRY_LINE_COUNT: number;
    VERTICAL_SIZE_OF_DIGIT_IN_AN_ENTRY: number;
    MAXIMUM_NUMBER_OF_ENTRIES: number;

    respectAll(lines: string[]): void;

    shouldRespectMaxNumberOfEntry(lines: string[]): void;

    shouldRespectEntryLineCount(lines: string[]): void;

    shouldRespectBlankLineAtEntryEnd(lines: string[]): void;

    shouldRespectMaxNumberOfDigitByEntry(numberOfDigitInEntry: number): void;
}

export interface DigitRepresentationByNumber {
    [key: number]: string[];
}

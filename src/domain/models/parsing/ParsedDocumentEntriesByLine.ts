export abstract class ParsedDocumentEntriesByLine {
    constructor(public readonly entriesByLine: string[] = []) {
        this.entriesByLine = entriesByLine;
    }

    toString(): string {
        return this.entriesByLine.join('');
    }
}

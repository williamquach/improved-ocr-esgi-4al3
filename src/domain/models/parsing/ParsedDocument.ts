import { ParsedDocumentEntriesByLine } from './ParsedDocumentEntriesByLine';

export abstract class ParsedDocument {
    constructor(public readonly entriesByLine: ParsedDocumentEntriesByLine[]) {
        this.entriesByLine = entriesByLine;
    }

    push(entry: ParsedDocumentEntriesByLine): void {
        this.entriesByLine.push(entry);
    }
}

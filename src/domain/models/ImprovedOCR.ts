import { ParsedDocumentEntriesByLine } from './parsing/ParsedDocumentEntriesByLine';
import { CheckSumValidation } from '../enums/CheckSumValidation';
import { ParsedDocumentWithFilePath } from '../../infrastructure/models/parsing/ParsedDocumentWithFilePath';
import { ContentFileWithFilePath } from '../../infrastructure/models/parsing/ContentFileWithFilePath';
import { OutputFilePaths } from '../../infrastructure/models/output/OutputFilePaths';

export interface ImprovedOCR {
    readDocuments(filePaths: string[]): ContentFileWithFilePath[];

    parseDocuments(fileContent: ContentFileWithFilePath[]): ParsedDocumentWithFilePath[];

    checkSum(currentLineEntries: ParsedDocumentEntriesByLine): CheckSumValidation;

    produceFilesFromEntryFiles(
        filePath: string,
        parsedDocumentWithFilePath: ParsedDocumentWithFilePath[]
    ): OutputFilePaths;
}

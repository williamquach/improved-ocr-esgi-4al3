export interface FileServiceCommand {
    writeInFile(filePath: string, fileName: string, content: string): string;

    removeFilesInOutputFolder(folderPath: string): void;
}

export interface FileServiceQuery {
    getFileContent(path: string): string;
}

export interface FileService extends FileServiceCommand, FileServiceQuery {}

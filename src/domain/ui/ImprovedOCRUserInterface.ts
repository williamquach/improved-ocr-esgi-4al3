import { OutputFilePaths } from '../../infrastructure/models/output/OutputFilePaths';

export interface ImprovedOCRUserInterface {
    start(): Promise<OutputFilePaths | undefined>;

    askUserFilePaths(): Promise<string[]>;

    askUserFileOutputFolderPath(): Promise<string>;

    doesUserWantsToGroupFiles(): Promise<boolean>;

    readFileAndWriteOutput(
        filePaths: string[],
        outputFolderPath: string,
        wantsToGroupFile: boolean
    ): Promise<OutputFilePaths | undefined>;

    stop(): void;

    restart(): void;
}

import { ImprovedOCRUserInterface } from './domain/ui/ImprovedOCRUserInterface';
import { ImprovedOCRUserInterfaceCLI } from './infrastructure/ui/ImprovedOCRUserInterface.CLI';
import { FileService } from './domain/services/FileService';
import { FSFileService } from './infrastructure/services/FSFileService';
import { ParsedDocumentEntriesByLineBuilder } from './domain/builder/ParsedDocumentEntriesByLineBuilder';
import { ParsedDocumentFileEntriesByLineBuilder } from './infrastructure/builder/ParsedDocumentFileEntriesByLineBuilder';
import { ParsedDocumentBuilder } from './domain/builder/ParsedDocumentBuilder';
import { ParsedDocumentFileBuilder } from './infrastructure/builder/ParsedDocumentFileBuilder';
import { FileImprovedOCRRules } from './infrastructure/models/FileImprovedOCRRules';

console.log('=== Start Improved ocr ===');

(async () => {
    let fileService: FileService = new FSFileService();
    let parsedDocumentEntriesByLineBuilder: ParsedDocumentEntriesByLineBuilder =
        new ParsedDocumentFileEntriesByLineBuilder();
    let parsedDocumentBuilder: ParsedDocumentBuilder = new ParsedDocumentFileBuilder(
        parsedDocumentEntriesByLineBuilder
    );

    const userInterface: ImprovedOCRUserInterface = new ImprovedOCRUserInterfaceCLI(
        new FileImprovedOCRRules(),
        fileService,
        parsedDocumentBuilder
    );
    await userInterface.start();
})();

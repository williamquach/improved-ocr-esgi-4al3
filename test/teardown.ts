import { FileService } from '../src/domain/services/FileService';
import { FSFileService } from '../src/infrastructure/services/FSFileService';
import path from 'path';

module.exports = async function () {
    let fileService: FileService = new FSFileService();
    fileService.removeFilesInOutputFolder(path.join(__dirname, 'ocr/resources/output'));
};

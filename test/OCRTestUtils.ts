import { ImprovedOCRRules } from '../src/domain/models/ImprovedOCRRules';
import { FileService } from '../src/domain/services/FileService';
import { FSFileService } from '../src/infrastructure/services/FSFileService';
import { ParsedDocumentEntriesByLineBuilder } from '../src/domain/builder/ParsedDocumentEntriesByLineBuilder';
import { ParsedDocumentFileEntriesByLineBuilder } from '../src/infrastructure/builder/ParsedDocumentFileEntriesByLineBuilder';
import { ParsedDocumentBuilder } from '../src/domain/builder/ParsedDocumentBuilder';
import { ParsedDocumentFileBuilder } from '../src/infrastructure/builder/ParsedDocumentFileBuilder';
import { FileImprovedOCR } from '../src/infrastructure/models/FileImprovedOCR';
import { ImprovedOCR } from '../src/domain/models/ImprovedOCR';
import path from 'path';

export interface OCRWrapper {
    fileService: FileService;
    improvedOCR: ImprovedOCR;
    parsedDocumentBuilder: ParsedDocumentBuilder;
}

export class OCRTestWrapper implements OCRWrapper {
    constructor(
        public fileService: FileService,
        public improvedOCR: ImprovedOCR,
        public parsedDocumentBuilder: ParsedDocumentBuilder
    ) {}

    removeFilesInOutputFolder() {
        this.fileService.removeFilesInOutputFolder(path.join(__dirname, 'ocr/resources/output'));
    }
}

export function generateOCRTestWrapper(rules: ImprovedOCRRules): OCRTestWrapper {
    let fileService: FileService = new FSFileService();
    let parsedDocumentEntriesByLineBuilder: ParsedDocumentEntriesByLineBuilder =
        new ParsedDocumentFileEntriesByLineBuilder();
    let parsedDocumentBuilder: ParsedDocumentBuilder = new ParsedDocumentFileBuilder(
        parsedDocumentEntriesByLineBuilder
    );
    return new OCRTestWrapper(
        fileService,
        new FileImprovedOCR(rules, fileService, parsedDocumentBuilder),
        parsedDocumentBuilder
    );
}

import path from 'path';
import { ImprovedOCR } from '../../src/domain/models/ImprovedOCR';
import { FileImprovedOCRRules } from '../../src/infrastructure/models/FileImprovedOCRRules';
import { ImprovedOCRRules } from '../../src/domain/models/ImprovedOCRRules';
import { generateOCRTestWrapper, OCRTestWrapper } from '../OCRTestUtils';

describe('Improved OCR can detect unknown digit', () => {
    const rules: ImprovedOCRRules = new FileImprovedOCRRules();
    let testWrapper: OCRTestWrapper;
    let improvedOCR: ImprovedOCR;

    beforeEach(() => {
        testWrapper = generateOCRTestWrapper(rules);
        improvedOCR = testWrapper.improvedOCR;
    });

    it('should be able to detect unknown digit, replace with "?" in output file and write "ILL in file"', () => {
        const inputFilePathAndName = path.join(__dirname, 'resources/check-sum/unknown-digit.txt');
        const fileContent = improvedOCR.readDocuments([inputFilePathAndName]);
        const parsedDocuments = improvedOCR.parseDocuments(fileContent);

        const outputFilePath = path.join(__dirname, 'resources/output/');
        const outputFullFilePath = improvedOCR.produceFilesFromEntryFiles(outputFilePath, [
            {
                filePath: inputFilePathAndName,
                parsedDocument: parsedDocuments[0].parsedDocument,
            },
        ]);
        expect(testWrapper.fileService.getFileContent(outputFullFilePath.filePaths[0].filePath)).toStrictEqual(
            '12?13678? ILL\n'
        );
    });
});

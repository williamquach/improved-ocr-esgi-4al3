import path from 'path';
import { ImprovedOCR } from '../../src/domain/models/ImprovedOCR';
import { FileImprovedOCRRules } from '../../src/infrastructure/models/FileImprovedOCRRules';
import { ImprovedOCRRules } from '../../src/domain/models/ImprovedOCRRules';
import { generateOCRTestWrapper, OCRTestWrapper } from '../OCRTestUtils';

describe('Improved OCR produces output file', () => {
    const rules: ImprovedOCRRules = new FileImprovedOCRRules();
    let testWrapper: OCRTestWrapper;
    let improvedOCR: ImprovedOCR;

    beforeEach(() => {
        testWrapper = generateOCRTestWrapper(rules);
        improvedOCR = testWrapper.improvedOCR;
    });

    it('should produce two files for each entry files', () => {
        const firstInputFilePathAndName = path.join(
            __dirname,
            'resources/multiple-output-files/first-multiple-output-file.txt'
        );
        const secondInputFilePathAndName = path.join(
            __dirname,
            'resources/multiple-output-files/second-multiple-output-file.txt'
        );
        const filesContent = improvedOCR.readDocuments([firstInputFilePathAndName, secondInputFilePathAndName]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);

        const outputFilePath = path.join(__dirname, 'resources/output/');
        const outputFullFilePath = improvedOCR.produceFilesFromEntryFiles(outputFilePath, [
            {
                filePath: firstInputFilePathAndName,
                parsedDocument: parsedDocuments[0].parsedDocument,
            },
            {
                filePath: secondInputFilePathAndName,
                parsedDocument: parsedDocuments[1].parsedDocument,
            },
        ]);

        expect(testWrapper.fileService.getFileContent(outputFullFilePath.filePaths[0].filePath)).toStrictEqual(
            '457508000\n664371495 ERR\n'
        );
        expect(testWrapper.fileService.getFileContent(outputFullFilePath.filePaths[1].filePath)).toStrictEqual(
            '457508000\n664371495 ERR\n12?13678? ILL\n'
        );
    });
});

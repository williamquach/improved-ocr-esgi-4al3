import path from 'path';
import { ParsedDocumentBuilder } from '../../../src/domain/builder/ParsedDocumentBuilder';
import { FileService } from '../../../src/domain/services/FileService';
import { ParsedDocumentFileBuilder } from '../../../src/infrastructure/builder/ParsedDocumentFileBuilder';
import { FSFileService } from '../../../src/infrastructure/services/FSFileService';
import { FileImprovedOCRRules } from '../../../src/infrastructure/models/FileImprovedOCRRules';
import { ImprovedOCRRules } from '../../../src/domain/models/ImprovedOCRRules';
import { ParsedDocumentEntriesByLineBuilder } from '../../../src/domain/builder/ParsedDocumentEntriesByLineBuilder';
import { ParsedDocumentFileEntriesByLineBuilder } from '../../../src/infrastructure/builder/ParsedDocumentFileEntriesByLineBuilder';
import { ImprovedOCRUseCases } from '../../../src/domain/use-cases/ocr/ImprovedOCRUseCases';
import { FileImprovedOCRUseCases } from '../../../src/infrastructure/use-cases/ocr/FileImprovedOCRUseCases';
import { OutputFilePaths } from '../../../src/infrastructure/models/output/OutputFilePaths';

function getAuthorizedFilePath(outputFilePaths: OutputFilePaths): string {
    const found = outputFilePaths.filePaths.find(currentOutputFilePath =>
        currentOutputFilePath.filePath.includes('authorized')
    );
    return found?.filePath ?? '';
}

function getErroredFilePath(outputFilePaths: OutputFilePaths): string {
    const found = outputFilePaths.filePaths.find(currentOutputFilePath =>
        currentOutputFilePath.filePath.includes('errored')
    );
    return found?.filePath ?? '';
}

function getUnknownFilePath(outputFilePaths: OutputFilePaths): string {
    const found = outputFilePaths.filePaths.find(currentOutputFilePath =>
        currentOutputFilePath.filePath.includes('unknown')
    );
    return found?.filePath ?? '';
}

describe('Improved OCR produces output file', () => {
    let rules: ImprovedOCRRules = new FileImprovedOCRRules();
    let fileService: FileService = new FSFileService();
    let parsedDocumentEntriesByLineBuilder: ParsedDocumentEntriesByLineBuilder =
        new ParsedDocumentFileEntriesByLineBuilder();
    let parsedDocumentBuilder: ParsedDocumentBuilder = new ParsedDocumentFileBuilder(
        parsedDocumentEntriesByLineBuilder
    );
    let improvedOCRUseCases: ImprovedOCRUseCases;

    beforeEach(() => {
        improvedOCRUseCases = new FileImprovedOCRUseCases(rules, fileService, parsedDocumentBuilder, true);
    });

    it('should produce two files grouped in their directories based on output code', () => {
        const outputFilePaths = improvedOCRUseCases.readFileAndWriteOutput(
            [
                path.join(__dirname, '../resources/grouped-output-files/with-multiple-check-sum-cases.txt'),
                path.join(__dirname, '../resources/grouped-output-files/with-multiple-check-sum-cases-second.txt'),
            ],
            path.join(__dirname, '../resources/output')
        );

        expect(fileService.getFileContent(getAuthorizedFilePath(outputFilePaths))).toStrictEqual(
            '457508000\n457508000\n'
        );
        expect(fileService.getFileContent(getErroredFilePath(outputFilePaths))).toStrictEqual('664371495 ERR\n');
        expect(fileService.getFileContent(getUnknownFilePath(outputFilePaths))).toStrictEqual(
            '12?13678? ILL\n12?13678? ILL\n12?13678? ILL\n'
        );
    });
});

import path from 'path';
import { ImprovedOCR } from '../../src/domain/models/ImprovedOCR';
import { FileImprovedOCRRules } from '../../src/infrastructure/models/FileImprovedOCRRules';
import { ImprovedOCRRules } from '../../src/domain/models/ImprovedOCRRules';
import { generateOCRTestWrapper, OCRTestWrapper } from '../OCRTestUtils';
import { IncorrectNumberOfEntriesError } from '../../src/domain/errors/ocr/IncorrectNumberOfEntriesError';
import { EntryDigitCountError } from '../../src/domain/errors/ocr/EntryDigitCountError';
import { ImprovedOCRInputFormatError } from '../../src/domain/errors/ocr/ImprovedOCRInputFormatError';

describe('Improved OCR can detect unknown digit', () => {
    const rules: ImprovedOCRRules = new FileImprovedOCRRules();
    let testWrapper: OCRTestWrapper;
    let improvedOCR: ImprovedOCR;

    beforeEach(() => {
        testWrapper = generateOCRTestWrapper(rules);
        improvedOCR = testWrapper.improvedOCR;
    });

    it(`should throw an error when there are more than ${rules.MAX_NUMBER_OF_DIGIT_IN_AN_ENTRY} digits by entry`, () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/incorrect-format/more-than-expected-digits.txt'),
        ]);
        expect(() => {
            improvedOCR.parseDocuments(filesContent);
        }).toThrow(EntryDigitCountError);
    });

    it(`should throw a parsing error since file contains more than ${rules.MAXIMUM_NUMBER_OF_ENTRIES} entries`, () => {
        const inputFilePathAndName = path.join(__dirname, 'resources/incorrect-format/more-than-100-entries.txt');
        const filesContent = improvedOCR.readDocuments([inputFilePathAndName]);

        expect(() => {
            improvedOCR.parseDocuments(filesContent);
        }).toThrow(IncorrectNumberOfEntriesError);
    });

    it('should throw a parsing error since file contains 2 blanks between entries', () => {
        const inputFilePathAndName = path.join(
            __dirname,
            'resources/incorrect-format/incorrect-format-number-of-lines.txt'
        );
        const filesContent = improvedOCR.readDocuments([inputFilePathAndName]);

        expect(() => {
            improvedOCR.parseDocuments(filesContent);
        }).toThrow(ImprovedOCRInputFormatError);
    });
});

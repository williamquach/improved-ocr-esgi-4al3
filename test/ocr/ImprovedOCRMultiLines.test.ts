import path from 'path';
import { ImprovedOCR } from '../../src/domain/models/ImprovedOCR';
import { FileImprovedOCRRules } from '../../src/infrastructure/models/FileImprovedOCRRules';
import { ImprovedOCRRules } from '../../src/domain/models/ImprovedOCRRules';
import { generateOCRTestWrapper, OCRTestWrapper } from '../OCRTestUtils';

describe('Improved OCR can handle multi-lines files', () => {
    const rules: ImprovedOCRRules = new FileImprovedOCRRules();
    let testWrapper: OCRTestWrapper;
    let improvedOCR: ImprovedOCR;

    beforeEach(() => {
        testWrapper = generateOCRTestWrapper(rules);
        improvedOCR = testWrapper.improvedOCR;
    });

    it('should be able to parse a multi-line entry file', () => {
        const inputFilePathAndName = path.join(__dirname, 'resources/multi-line/multi-lines.txt');
        const filesContent = improvedOCR.readDocuments([inputFilePathAndName]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);

        const outputFilePath = path.join(__dirname, 'resources/output/');
        const outputFullFilePath = improvedOCR.produceFilesFromEntryFiles(outputFilePath, [
            {
                filePath: inputFilePathAndName,
                parsedDocument: parsedDocuments[0].parsedDocument,
            },
        ]);
        expect(testWrapper.fileService.getFileContent(outputFullFilePath.filePaths[0].filePath)).toStrictEqual(
            '457508000\n664371495 ERR\n'
        );
    });

    it('should be able to parse a multi-line entry file and handle ERR & ILL', () => {
        const inputFilePathAndName = path.join(__dirname, 'resources/multi-line/multi-lines-with-err-and-ill.txt');
        const filesContent = improvedOCR.readDocuments([inputFilePathAndName]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);

        const outputFilePath = path.join(__dirname, 'resources/output/');
        const outputFullFilePath = improvedOCR.produceFilesFromEntryFiles(outputFilePath, [
            {
                filePath: inputFilePathAndName,
                parsedDocument: parsedDocuments[0].parsedDocument,
            },
        ]);
        expect(testWrapper.fileService.getFileContent(outputFullFilePath.filePaths[0].filePath)).toStrictEqual(
            '457508000\n664371495 ERR\n12?13678? ILL\n'
        );
    });
});

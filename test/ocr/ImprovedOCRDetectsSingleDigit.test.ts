import path from 'path';
import { ImprovedOCR } from '../../src/domain/models/ImprovedOCR';
import { FileImprovedOCRRules } from '../../src/infrastructure/models/FileImprovedOCRRules';
import { ImprovedOCRRules } from '../../src/domain/models/ImprovedOCRRules';
import { generateOCRTestWrapper, OCRTestWrapper } from '../OCRTestUtils';

describe('Improved OCR detects single digit in file', () => {
    const rules: ImprovedOCRRules = new FileImprovedOCRRules();
    let testWrapper: OCRTestWrapper;
    let improvedOCR: ImprovedOCR;

    beforeEach(() => {
        testWrapper = generateOCRTestWrapper(rules);
        improvedOCR = testWrapper.improvedOCR;
    });

    it('should detect a single 0', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-zero-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['0']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });

    it('should detect a single 1', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-one-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['1']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });

    it('should detect a single 2', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-two-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['2']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });

    it('should detect a single 3', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-three-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['3']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });

    it('should detect a single 4', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-four-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['4']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });

    it('should detect a single 5', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-five-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['5']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });

    it('should detect a single 6', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-six-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['6']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });

    it('should detect a single 7', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-seven-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['7']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });

    it('should detect a single 8', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-eight-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['8']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });

    it('should detect a single 9', () => {
        const filesContent = improvedOCR.readDocuments([
            path.join(__dirname, 'resources/simple-digit/simple-nine-digit.txt'),
        ]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder.setEntries(['9']).build();
        expect(parsedDocuments[0].parsedDocument).toStrictEqual(expected);
    });
});

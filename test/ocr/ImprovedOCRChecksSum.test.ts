import path from 'path';
import { CheckSumValidation } from '../../src/domain/enums/CheckSumValidation';
import { generateOCRTestWrapper, OCRTestWrapper } from '../OCRTestUtils';
import { ImprovedOCR } from '../../src/domain/models/ImprovedOCR';
import { ImprovedOCRRules } from '../../src/domain/models/ImprovedOCRRules';
import { FileImprovedOCRRules } from '../../src/infrastructure/models/FileImprovedOCRRules';

describe('Improved OCR produces output file', () => {
    const rules: ImprovedOCRRules = new FileImprovedOCRRules();
    let testWrapper: OCRTestWrapper;
    let improvedOCR: ImprovedOCR;

    beforeEach(() => {
        testWrapper = generateOCRTestWrapper(rules);
        improvedOCR = testWrapper.improvedOCR;
    });

    it('should produce truthy check sum with a correct entry', () => {
        const inputFilePathAndName = path.join(__dirname, 'resources/check-sum/check-sum-correct.txt');
        const filesContent = improvedOCR.readDocuments([inputFilePathAndName]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        expect(improvedOCR.checkSum(parsedDocuments[0].parsedDocument.entriesByLine[0])).toEqual(
            CheckSumValidation.VALID
        );
    });

    it('should not write ERR in file when truthy check sum', () => {
        const inputFilePathAndName = path.join(__dirname, 'resources/check-sum/check-sum-correct.txt');
        const filesContent = improvedOCR.readDocuments([inputFilePathAndName]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);

        const outputFilePath = path.join(__dirname, 'resources/output/');
        const outputFullFilePaths = improvedOCR.produceFilesFromEntryFiles(outputFilePath, [
            {
                filePath: inputFilePathAndName,
                parsedDocument: parsedDocuments[0].parsedDocument,
            },
        ]);

        expect(testWrapper.fileService.getFileContent(outputFullFilePaths.filePaths[0].filePath)).toStrictEqual(
            '457508000\n'
        );
    });

    it('should produce falsy check sum with an incorrect entry', () => {
        const inputFilePathAndName = path.join(__dirname, 'resources/check-sum/check-sum-incorrect.txt');
        const filesContent = improvedOCR.readDocuments([inputFilePathAndName]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);
        expect(improvedOCR.checkSum(parsedDocuments[0].parsedDocument.entriesByLine[0])).toEqual(
            CheckSumValidation.INVALID
        );
    });

    it('should write ERR in file when falsy check sum', () => {
        const inputFilePathAndName = path.join(__dirname, 'resources/check-sum/check-sum-incorrect.txt');
        const filesContent = improvedOCR.readDocuments([inputFilePathAndName]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);

        const outputFilePath = path.join(__dirname, 'resources/output/');
        const outputFullFilePath = improvedOCR.produceFilesFromEntryFiles(outputFilePath, [
            {
                filePath: inputFilePathAndName,
                parsedDocument: parsedDocuments[0].parsedDocument,
            },
        ]);

        expect(testWrapper.fileService.getFileContent(outputFullFilePath.filePaths[0].filePath)).toStrictEqual(
            '664371495 ERR\n'
        );
    });
});

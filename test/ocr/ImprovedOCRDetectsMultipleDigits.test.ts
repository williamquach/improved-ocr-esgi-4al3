import path from 'path';
import { ImprovedOCR } from '../../src/domain/models/ImprovedOCR';
import { generateOCRTestWrapper, OCRTestWrapper } from '../OCRTestUtils';
import { ImprovedOCRRules } from '../../src/domain/models/ImprovedOCRRules';
import { FileImprovedOCRRules } from '../../src/infrastructure/models/FileImprovedOCRRules';

describe('Improved OCR detects multiple digits in file', () => {
    const rules: ImprovedOCRRules = new FileImprovedOCRRules();
    let testWrapper: OCRTestWrapper;
    let improvedOCR: ImprovedOCR;

    beforeEach(() => {
        testWrapper = generateOCRTestWrapper(rules);
        improvedOCR = testWrapper.improvedOCR;
    });

    it('should detect max numbers from 1 to 9', () => {
        const filesContent = improvedOCR.readDocuments([path.join(__dirname, 'resources/max-number-of-digits.txt')]);
        const result = improvedOCR.parseDocuments(filesContent);
        const expected = testWrapper.parsedDocumentBuilder
            .setEntries(['1', '2', '3', '4', '5', '6', '7', '8', '9'])
            .build();
        expect(result[0].parsedDocument).toStrictEqual(expected);
    });
});

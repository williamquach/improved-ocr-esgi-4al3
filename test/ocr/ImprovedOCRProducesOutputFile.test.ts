import path from 'path';
import { ImprovedOCR } from '../../src/domain/models/ImprovedOCR';
import { FileImprovedOCRRules } from '../../src/infrastructure/models/FileImprovedOCRRules';
import { ImprovedOCRRules } from '../../src/domain/models/ImprovedOCRRules';
import { generateOCRTestWrapper, OCRTestWrapper } from '../OCRTestUtils';

describe('Improved OCR produces output file', () => {
    const rules: ImprovedOCRRules = new FileImprovedOCRRules();
    let testWrapper: OCRTestWrapper;
    let improvedOCR: ImprovedOCR;

    beforeEach(() => {
        testWrapper = generateOCRTestWrapper(rules);
        improvedOCR = testWrapper.improvedOCR;
    });

    it('should produce a file with 1 to 9 entries', () => {
        const inputFilePathAndName = path.join(__dirname, 'resources/max-number-of-digits.txt');
        const filesContent = improvedOCR.readDocuments([inputFilePathAndName]);
        const parsedDocuments = improvedOCR.parseDocuments(filesContent);

        const outputFilePath = path.join(__dirname, 'resources/output/');
        const outputFullFilePath = improvedOCR.produceFilesFromEntryFiles(outputFilePath, [
            {
                filePath: inputFilePathAndName,
                parsedDocument: parsedDocuments[0].parsedDocument,
            },
        ]);

        expect(testWrapper.fileService.getFileContent(outputFullFilePath.filePaths[0].filePath)).toStrictEqual(
            '123456789\n'
        );
    });
});
